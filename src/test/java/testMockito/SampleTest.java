package testMockito;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.modules.junit4.PowerMockRunner;
import junit.framework.Assert;

import static org.mockito.Matchers.any;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.easymock.PowerMock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.powermock.reflect.Whitebox;



@RunWith(PowerMockRunner.class)
@PrepareForTest( { Sample.Content.class })
public class SampleTest {

	
	@Test
	public void testContent() throws Exception {
	
		mockStatic(Sample.Content.class);
		when(Sample.Content.printSample("printMe")).thenReturn("new String");
		
	    Assert.assertEquals(Sample.Content.printSample("printMe"),"new String");
		
	}
	


}

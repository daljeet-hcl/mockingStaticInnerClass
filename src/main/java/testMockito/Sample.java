package testMockito;

public class Sample {

 public static class  Content{
	 
	public static String printSample(String obj)
	{
		System.out.println("this class prints the String passed: "+ obj);
		return new String("hello ");
	}
 }
 
 
 public static void main(String[] args)
 {
	 Sample.Content.printSample("daljeet");
 }
}
